﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudentManager.Models
{
    public class StudentBook
    {
        public int Id { get; set; }
        public string StudentId { get; set; }
        public virtual Student Student { get; set; }
        public int BookId { get; set; }
        public virtual Book Book { get; set; }
        [JsonIgnore]
        public DateTime BorrowDate { get; set; }

        [NotMapped]
        public string BorrowDateString
        {
            get
            {
                return BorrowDate.ToString("dd/MM/yyyy hh:mm");
            }
        }

        [JsonIgnore]
        public DateTime? ReturnDate { get; set; }

        [NotMapped]
        public string ReturnDateString
        {
            get
            {
                if (ReturnDate.HasValue)
                    return ReturnDate.Value.ToString("dd/MM/yyyy hh:mm");
                return "";
            }
        }
    }
}