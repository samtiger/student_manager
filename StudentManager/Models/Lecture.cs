﻿using System;
using System.Collections.Generic;

namespace StudentManager.Models
{
    public class Lecture : BaseEntity
    {
        public DateTime Time { get; set; }

        public string LecturerName { get; set; }

        public bool IsLab { get; set; }
    }
}