﻿using System.Collections.Generic;

namespace StudentManager.Models
{
    public class Student
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string RFID { get; set; }

        //public virtual List<LectureStudent> Lectures { get; set; }

        //public virtual List<StudentBook> Books { get; set; }
    }
}