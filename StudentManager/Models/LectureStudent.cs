﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudentManager.Models
{
    public class LectureStudent
    {
        public int Id { get; set; }
        public int LectureId { get; set; }
        public virtual Lecture Lecture { get; set; }

        public string StudentId { get; set; }
        public virtual Student Student { get; set; }

        [JsonIgnore]
        public DateTime? CheckInTime { get; set; }

        [NotMapped]
        public string CheckInTimeString
        {
            get
            {
                if (CheckInTime.HasValue)
                {
                    return CheckInTime.Value.ToString("dd/MM/yyyy hh:mm");
                }
                return "";
            }
        }
        public bool Attended { get; set; }

    }
}