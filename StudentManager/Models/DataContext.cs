﻿using System.Data.Entity;

namespace StudentManager.Models
{
    public class DataContext : DbContext
    {
        public DataContext() : base("StudentManager")
        {
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Lecture> Lectures { get; set; }
        public DbSet<LectureStudent> LectureStudents { get; set; }
        public DbSet<StudentBook> StudentBooks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LectureStudent>()
                .HasRequired(m => m.Student)
                .WithMany()
                .HasForeignKey(e => e.StudentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LectureStudent>()
                .HasRequired(m => m.Lecture)
                .WithMany()
                .HasForeignKey(e => e.LectureId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StudentBook>()
                .HasRequired(m => m.Student)
                .WithMany()
                .HasForeignKey(e => e.StudentId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StudentBook>()
                .HasRequired(m => m.Book)
                .WithMany()
                .HasForeignKey(e => e.BookId)
                .WillCascadeOnDelete(false);

        }
    }
}