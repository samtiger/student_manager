﻿using StudentManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StudentManager.Controllers
{
    public class HomeController : Controller
    {
        DataContext db;

        public HomeController()
        {
            db = new DataContext();
        }

        public ActionResult Index(string lectureName = "")
        {
            var lectures = db.LectureStudents
                .Where(m => (string.IsNullOrEmpty(lectureName) || (m.Lecture.Name.Contains(lectureName))) && !m.Lecture.IsLab)
                .ToList();
            return View(lectures);
        }

        public ActionResult Lab(string labName = "")
        {
            var lectures = db.LectureStudents
                .Where(m => (string.IsNullOrEmpty(labName) || (m.Lecture.Name.Contains(labName))) && m.Lecture.IsLab)
                .ToList();
            return View(lectures);
        }

        public ActionResult Library(string studentId = "")
        {
            if (!string.IsNullOrEmpty(studentId))
            {
                var books = db.StudentBooks
                .Where(m => string.IsNullOrEmpty(studentId) || m.Student.Id.Contains(studentId))
                .ToList();
                return View(books);
            }
            return View(new List<StudentBook>());
        }

        public ActionResult Add(string c, string user_id = "", string rf_id = "", int book_id = 0)
        {
            c = c.ToLower();
            if (c == "a" || c == "d")
            {
                var student = db.Students.FirstOrDefault(m => m.Id == user_id || m.RFID == rf_id);
                var lectureStudent = db.LectureStudents.FirstOrDefault(m => (m.Student.RFID == rf_id || m.StudentId == user_id) && m.Lecture.Time.Hour == DateTime.Now.Hour);
                if (lectureStudent != null)
                {
                    lectureStudent.CheckInTime = DateTime.Now;
                    lectureStudent.Attended = (DateTime.Now - lectureStudent.Lecture.Time).Minutes <= 15;

                    db.SaveChanges();
                }
                return Json(student, JsonRequestBehavior.AllowGet);
            }

            if (c == "b")
            {
                var student = db.Students.FirstOrDefault(m => m.RFID == rf_id || m.Id == user_id);
                db.StudentBooks.Add(new StudentBook
                {
                    BookId = book_id,
                    BorrowDate = DateTime.Now,
                    ReturnDate = null,
                    StudentId = student.Id
                });

                db.SaveChanges();

                return Json(student, JsonRequestBehavior.AllowGet);
            }

            if (c == "c")
            {
                var student = db.Students.FirstOrDefault(m => m.RFID == rf_id || m.Id == user_id);
                var studentBook = db.StudentBooks.FirstOrDefault(m => m.StudentId == student.Id && m.BookId == book_id);

                //studentBook.ReturnDate = DateTime.Now;

                int fee = 0;

                var days = (studentBook.BorrowDate - DateTime.Now).Days;

                if (days > 2)
                    fee = days - 2;

                db.StudentBooks.Remove(studentBook);

                db.SaveChanges();

                var response = new StudentFee
                {
                    Fee = fee,
                    StudentId = studentBook.Student.Id,
                    StudentName = studentBook.Student.Name
                };

                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}