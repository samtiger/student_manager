namespace StudentManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AuthorId = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Lectures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Time = c.DateTime(nullable: false),
                        LecturerName = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.LectureStudents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LectureId = c.Int(nullable: false),
                        StudentId = c.String(nullable: false, maxLength: 128),
                        CheckInTime = c.DateTime(),
                        Attended = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lectures", t => t.LectureId)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .Index(t => t.LectureId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        RFID = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StudentBooks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StudentId = c.String(nullable: false, maxLength: 128),
                        BookId = c.Int(nullable: false),
                        BorrowDate = c.DateTime(nullable: false),
                        ReturnDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Books", t => t.BookId)
                .ForeignKey("dbo.Students", t => t.StudentId)
                .Index(t => t.StudentId)
                .Index(t => t.BookId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StudentBooks", "StudentId", "dbo.Students");
            DropForeignKey("dbo.StudentBooks", "BookId", "dbo.Books");
            DropForeignKey("dbo.LectureStudents", "StudentId", "dbo.Students");
            DropForeignKey("dbo.LectureStudents", "LectureId", "dbo.Lectures");
            DropIndex("dbo.StudentBooks", new[] { "BookId" });
            DropIndex("dbo.StudentBooks", new[] { "StudentId" });
            DropIndex("dbo.LectureStudents", new[] { "StudentId" });
            DropIndex("dbo.LectureStudents", new[] { "LectureId" });
            DropTable("dbo.StudentBooks");
            DropTable("dbo.Students");
            DropTable("dbo.LectureStudents");
            DropTable("dbo.Lectures");
            DropTable("dbo.Books");
        }
    }
}
