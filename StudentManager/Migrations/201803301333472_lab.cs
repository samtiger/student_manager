namespace StudentManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lab : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lectures", "IsLab", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Lectures", "IsLab");
        }
    }
}
